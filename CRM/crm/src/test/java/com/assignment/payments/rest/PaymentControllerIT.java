package com.assignment.payments.rest;

import com.assignment.payments.entities.Payment;
import com.assignment.crm.services.user.IUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SuppressWarnings("unchecked")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerIT {

    @LocalServerPort
    int port;

    @Autowired
    IUserService IUserService;

    @Autowired
    MongoTemplate mongoTemplate;

    private String url;
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    HttpEntity entity = new HttpEntity(headers);

    @BeforeEach
    public void setup() {
        url = "http://localhost:" + port +"/api";
        mongoTemplate.dropCollection("payment");
    }

    @Test
    public void retrieveCountOfPayments(){

        IUserService.save( new Payment(1, new Date(), "Full Payment", 10.0, 1));

        ResponseEntity<Integer> response = restTemplate.getForEntity(
                url+ "/count/",
                Integer.class
        );

        assertEquals(1, response.getBody());
    }

    @Test
    public void getPaymentById(){
        Date date = new Date();
        String expected = new Payment(1, date, "Full Payment", 10.0, 1).toString();

        IUserService.save( new Payment(1, date, "Full Payment", 10.0, 1));

        ResponseEntity<Payment> response = restTemplate.getForEntity(
                url +"/id?id=1",
                Payment.class
        );

      assertEquals(expected, response.getBody().toString());
    }

    @Test
    public void getPaymentByType(){
        Date date = new Date();
        Payment paymentOne = new Payment(1, date, "FullPayment", 10.0, 1);
        Payment paymentTwo = new Payment(2, date, "FullPayment", 20.0, 2);

        IUserService.save(paymentOne);
        IUserService.save(paymentTwo);

        List<Payment> expected = new ArrayList<>();
        expected.add(paymentOne);
        expected.add(paymentTwo);

        ResponseEntity<List<Payment>> response = restTemplate.exchange(
                url+"/type?type=FullPayment",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<>() {
                }
        );

        assertEquals(expected.toString(), response.getBody().toString());
    }

    @Test
    public void postPaymentToDB(){
        Date date = new Date();
        Payment expected = new Payment(1, date, "FullPayment", 10.0, 1);
        entity = new HttpEntity(expected, headers);

        ResponseEntity response = restTemplate.exchange(
                url+"/addpayment",
                HttpMethod.POST,
                entity,
                ResponseEntity.class
        );
        assertEquals(201, response.getStatusCodeValue());

        ResponseEntity<Payment> responsetwo = restTemplate.getForEntity(
                url +"/id?id=1",
                Payment.class
        );

        assertEquals(expected.toString(), responsetwo.getBody().toString());
    }

    @Test
    public void getStatus(){

        ResponseEntity response = restTemplate.getForEntity(
                url +"/status/",
                ResponseEntity.class
        );

        assertEquals(200, response.getStatusCodeValue());
    }
}
