package com.assignment.crm.dao;

import com.assignment.crm.entities.User;
import com.assignment.crm.entities.UserJourneyLog;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class UserJourneyLogDAO implements IUserJourneyLogDAO {
    private MongoTemplate mongoTemplate;

    public UserJourneyLogDAO(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void updateUserJourneyLog(int id, String commentary) {
        String amendedCommentary = retrieveAllLogsById(id).get(0).getCommentary();
        amendedCommentary = amendedCommentary +  "Date of Action(s)" +LocalDate.now() +","+ commentary  ;
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Update update = new Update();
        update.set("date_of_activity", LocalDate.now());
        update.set("commentary",amendedCommentary);

        mongoTemplate.updateFirst(query, update, UserJourneyLog.class);

    }

    @Override
    public void addUserJourneyLogInitial(int id) {
        UserJourneyLog userJourneyLog = new UserJourneyLog(id, LocalDate.now(),"Added to system on " + LocalDate.now() +",");
        mongoTemplate.insert(userJourneyLog);
    }

    @Override
    public List<UserJourneyLog> retrieveAllLogsById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        List<UserJourneyLog> userJourneyLog = mongoTemplate.find(query, UserJourneyLog.class);
        return userJourneyLog;
    }
}