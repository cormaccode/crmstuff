package com.assignment.crm.services.logs;

import com.assignment.crm.dao.IUserJourneyLogDAO;
import com.assignment.crm.entities.UserJourneyLog;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserJourneyLogService implements IUserJourneyLogService {

    private IUserJourneyLogDAO iUserJourneyLogDAO;

    public UserJourneyLogService(IUserJourneyLogDAO iUserJourneyLogDAO){
        this.iUserJourneyLogDAO = iUserJourneyLogDAO;
    }

    @Override
    public List<UserJourneyLog> retrieveAllLogsById(int id) {
        return iUserJourneyLogDAO.retrieveAllLogsById(id);
    }
}
