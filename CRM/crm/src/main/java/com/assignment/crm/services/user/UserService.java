package com.assignment.crm.services.user;

import com.assignment.crm.dao.IUserDAO;
import com.assignment.crm.entities.User;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService implements IUserService  {

    private IUserDAO iUserDAO;

    public UserService(IUserDAO iUserDAO){
        this.iUserDAO =  iUserDAO;
    }

    @Override
    public User findById(int id) {
        return iUserDAO.findById(id);
    }

    @Override
    public List<User> findByFirstName(String firstname) {
        return iUserDAO.findByFirstName(firstname);
    }

    @Override
    public List<User> findByStatus(String status) {
        return iUserDAO.findByStatus(status);
    }

    @Override
    public void save(User user) {
        iUserDAO.save(user);
    }

    @Override
    public void updateUser(User user) {
        iUserDAO.updateUser(user);
    }
}