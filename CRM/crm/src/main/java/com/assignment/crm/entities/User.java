package com.assignment.crm.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class User {

    private int id;
    private String firstname;
    private String surname;
    private String gender;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private LocalDate date_of_birth;
    private String email_address;
    private String address;
    private String zipcode;
    private String phone_number;
    private String method_of_contact;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private LocalDate initial_date;
    private boolean email_sent;
    private boolean email_returned;
    private boolean phone_call_made;
    private boolean phone_call_returned;
    private String picture_location;
    private String status;
    private boolean active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDate date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getMethod_of_contact() {
        return method_of_contact;
    }

    public void setMethod_of_contact(String method_of_contact) {
        this.method_of_contact = method_of_contact;
    }

    public LocalDate getInitial_date() {
        return initial_date;
    }

    public void setInitial_date(LocalDate initial_date) {
        this.initial_date = initial_date;
    }

    public boolean isEmail_sent() {
        return email_sent;
    }

    public void setEmail_sent(boolean email_sent) {
        this.email_sent = email_sent;
    }

    public boolean isEmail_returned() {
        return email_returned;
    }

    public void setEmail_returned(boolean email_returned) {
        this.email_returned = email_returned;
    }

    public boolean isPhone_call_made() {
        return phone_call_made;
    }

    public void setPhone_call_made(boolean phone_call_made) {
        this.phone_call_made = phone_call_made;
    }

    public boolean isPhone_call_returned() {
        return phone_call_returned;
    }

    public void setPhone_call_returned(boolean phone_call_returned) {
        this.phone_call_returned = phone_call_returned;
    }

    public String getPicture_location() {
        return picture_location;
    }

    public void setPicture_location(String picture_location) {
        this.picture_location = picture_location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
