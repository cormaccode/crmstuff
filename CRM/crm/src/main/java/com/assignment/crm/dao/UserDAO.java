package com.assignment.crm.dao;

import com.assignment.crm.entities.User;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Change;
import org.javers.core.diff.Diff;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAO implements IUserDAO{

    private MongoTemplate mongoTemplate;
    private UserJourneyLogDAO userJourneyLogDAO;
    Javers javers = JaversBuilder.javers().build();

    public UserDAO(MongoTemplate mongoTemplate, UserJourneyLogDAO userJourneyLogDAO){
        this.mongoTemplate = mongoTemplate;
        this.userJourneyLogDAO = userJourneyLogDAO;
    }

    @Override
    public User findById(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        User user = mongoTemplate.findOne(query, User.class);
        return user;
    }

    @Override
    public List<User> findByFirstName(String firstname) {
        Query query = new Query();
        query.addCriteria(Criteria.where("firstname").is(firstname));
        List<User> userList = mongoTemplate.find(query, User.class);
        return userList;
    }

    @Override
    public List<User> findByStatus(String status) {
        Query query = new Query();
        query.addCriteria(Criteria.where("status").is(status));
        List<User> userList = mongoTemplate.find(query, User.class);
        return userList;
    }

    @Override
    public void save(User user) {
        mongoTemplate.insert(user);
        userJourneyLogDAO.addUserJourneyLogInitial(user.getId());
    }

    @Override
    public void updateUser(User user) {
        //search for current user on system to see what difference are

        User oldUser = findById(user.getId());
        oldUser.setDate_of_birth(user.getDate_of_birth());
        oldUser.setFirstname(user.getFirstname());
        oldUser.setSurname(user.getSurname());
        oldUser.setGender(user.getGender());
        oldUser.setInitial_date(user.getInitial_date());
        oldUser.setMethod_of_contact(user.getMethod_of_contact());
        oldUser.setPicture_location(user.getPicture_location());
        //assign the difference to commentary
        String commentary = String.valueOf(updatedFieldForLog(oldUser, user));
        userJourneyLogDAO.updateUserJourneyLog(user.getId(), commentary);

        //latest update
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(user.getId()));
        Update update = new Update();
        update.set("email_address", user.getEmail_address());
        update.set("address", user.getAddress());
        update.set("zipcode", user.getZipcode());
        update.set("phone_number", user.getPhone_number());
        update.set("email_sent", user.isEmail_sent());
        update.set("email_returned", user.isEmail_returned());
        update.set("phone_call_made", user.isPhone_call_made());
        update.set("phone_call_returned", user.isPhone_call_returned());
        update.set("status", user.getStatus());
        update.set("active", user.isActive());
        mongoTemplate.updateFirst(query, update, User.class);
    }

    private List<String> updatedFieldForLog(User oldMember, User newMember ){
       List<String>mylist = new ArrayList<>();

        Diff diff = javers.compare(oldMember, newMember);
        for(Change change:  diff.getChanges()) {
            System.out.println(change);
            mylist.add(change.toString());

        }
        return mylist;
    }


}
