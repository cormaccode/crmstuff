package com.assignment.crm.rest;

import com.assignment.crm.entities.User;
import com.assignment.crm.services.user.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController implements IUserController {
    private IUserService service;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    public UserController(IUserService service){
        this.service = service;
    }

    @Override
    @GetMapping("/id")
    public ResponseEntity<User> getUserById(int id) {
        logger.info("Calling method getUserById");
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @Override
    @GetMapping("/firstname")
    public ResponseEntity<List<User>> getUserByName(String firstname) {
        logger.info("Calling method getUserByName");
        return new ResponseEntity<>(service.findByFirstName(firstname), HttpStatus.OK);
    }

    @Override
    @GetMapping("/status")
    public ResponseEntity<List<User>> getUserByStatus(String status) {
        logger.info("Calling method getUserByStatus");
        return new ResponseEntity<>(service.findByStatus(status), HttpStatus.OK);
    }

    @Override
    @PostMapping("/adduser")
    public ResponseEntity addUser(User user) {
        logger.info("Calling method addUser");
        service.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Override
    @PostMapping("/updateuser")
    public ResponseEntity updateUser(User user) {
        logger.info("Calling method updateUser");
        service.updateUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}