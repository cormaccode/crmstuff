package com.assignment.crm.dao;

import com.assignment.crm.entities.UserJourneyLog;

import java.util.List;

public interface IUserJourneyLogDAO {
    void updateUserJourneyLog(int id, String commentary);
    void addUserJourneyLogInitial(int id);
    List<UserJourneyLog> retrieveAllLogsById(int id);
}
