package com.assignment.crm.rest;

import com.assignment.crm.entities.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface IUserController {

    @GetMapping("/id")
    ResponseEntity<User> getUserById(@RequestParam int id);

    @GetMapping("/firstname")
    ResponseEntity<List<User>> getUserByName(@RequestParam String firstname);

    @GetMapping("/status")
    ResponseEntity<List<User>> getUserByStatus(@RequestParam String status);

    @PostMapping("/adduser")
    ResponseEntity addUser(@RequestBody User user);

    @PostMapping("/updateuser")
    ResponseEntity updateUser(@RequestBody User user);
}
