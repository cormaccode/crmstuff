package com.assignment.crm.services.user;

import com.assignment.crm.entities.User;

import java.util.List;

public interface IUserService {
    User findById(int id);
    List<User> findByFirstName(String firstname);
    List<User> findByStatus(String status);
    void save(User user);
    void updateUser(User user);
}
