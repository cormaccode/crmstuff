package com.assignment.crm.rest;

import com.assignment.crm.entities.UserJourneyLog;
import com.assignment.crm.services.logs.IUserJourneyLogService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserJourneyLogController implements IUserJourneyLogController {
    private IUserJourneyLogService service;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    public UserJourneyLogController(IUserJourneyLogService service){
        this.service = service;
    }

    @Override
    @GetMapping("/allLogs")
    public ResponseEntity<List<UserJourneyLog>> getUserById(int id) {
        logger.info("Calling method getUserById");
        return new ResponseEntity<>(service.retrieveAllLogsById(id), HttpStatus.OK);
    }
}
